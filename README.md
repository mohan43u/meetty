# MeeTTY

A simple pty web frontend. This app is created to provide following windows for the purpose of conducting online training.

* IRC Chat Window (this window is to interact using irc)
* Presenter Terminal (this terminal window is to show Presenter tmux sharing session)
* Practice Terminal (uses usernamespace with fuse-overlayfs to create isolated temporary execution environment)

![MeeTTY screenshot](../docs/screenshots/MeeTTY.png)

## Starting MeeTTY Server

* MeeTTY server requires **guestroot** directory in order to create guest terminals.
* See **[Generating guestroot directory](#generating-guestroot-directory)** topic below for more details.

Let say, you created a guestroot directory called /home/presenter/ubuntu, you can link that guestroot into meety using following commandline,

```bash
ln -s /home/presenter/ubuntu /home/presenter/meetty/guest/root
```

Once guestroot is set, From meetty directory, run the following commands, these commands will start MeeTTY server on port 8000.

```bash
$ sudo ./limit/limit &
$ ./scripts/run.sh 0.0.0.0 8000
```

* To automate starting meetty, [look here](./systemd).
* See **[Modifying guestroot](#modifying-guestroot)** to know more about howto **chroot** into **guestroot**.
* If you want to change the init command of practice session, define **guestinitprog** and **guestinitargs** variables while running **run.sh**

following command shows howto define custom init command for practice session

```bash
   $ guestinitprog=/bin/busybox guestinitarg='ash -l' ./scripts/run.sh 0.0.0.0 8000
```

![MeeTTY server](../docs/screenshots/MeeTTY-server.png)

## Starting Presenter Terminal

**Presenter Terminal** is basically when we start **tmux** with a preconfigured setup in a terminal. MeeTTY provides a script to start **Presenter Terminal**.

* [tmux-share.sh](#tmux-sharesh)

### tmux-share.sh

The [tmux-share.sh](scripts/tmux-share.sh) script will start Presenter Terminal. You need to first login into the remote server where you are running [MeeTTY](scripts/run.sh) which is also the machine where users will connect. If you didn't clone meetty in the remote server, do so by using following command,

```bash
$ git clone https://gitlab.com/mohan43u/meetty.git
```

In the remote server, from the cloned meetty directory, run the following command

```bash
./scripts/tmux-share.sh
```

This will show Presenter Terminal

![MeeTTY Presenter Terminal](../docs/screenshots/MeeTTY-presenter.png)

There are two pane in this Presenter Terminal. The top pane contains **broadcast** TMUX session which will be broadcasted to all connected users. It is a TMUX session embedded inside another TMUX session. So **you have to press tmux prefix key two times (eg: 'ctrl-b ctrl-b') in the top pane to control Broadcast TMUX session**. The bottom pane is free to use terminal. it will not be shown to guests. To switch between panes **press 'ctrl-b o'**.

To disconnect from **Presenter Terminal**, press **ctrl-b d**.

#### Recording Presenter Terminal

Script [tmux-share.sh](scripts/tmux-share.sh) have the ability to  **Record Presenter Terminal Sessions**. To start recording, add **record=1** prefix like this,

```bash
record=1 ./scripts/tmux-share.sh
```

Recording is done using **script** unix command and output files of **script** command will be available under **~/.record.sh/** directory with timestamp in filenames. you can playback those files using **scriptreplay** unix command,

```bash
scriptreplay -m1 --timing=~/.record.sh/20200616155221-24417.timing ~/.record.sh/20200616155221-24417.typescript
```

## Generating guestroot directory

Run the following commands to create guestroot using debootstrap

```
$ cd ~
$ sudo debootstrap --components=main,universe,multiverse,restricted --variant=minbase focal ubuntu http://archive.ubuntu.com/ubuntu
$ ln -s ~/ubuntu ~/meetty/guest/root
```

Below steps uses skopeo and umoci wrapped in a [script](../scripts/genrootfs.sh). This script is capable of downloading any docker container from any docker registry.


```
$ cd ~
$ ./meetty/scripts/genrootfs.sh docker://ubuntu ubuntu
$ ln -s ~/ubuntu ~/meetty/guest/root
```

## Modifying guestroot

If you need to modify guestroot directory, use the following command to chroot into the guestroot to do your changes. Let say **~/ubuntu** is your guestroot directory, then following command will chroot into that directory.

```
$ sudo ./meetty/scripts/chroot-guest.sh ~/ubuntu
# apt update
# apt upgrade -y
# apt install build-essential
# exit
```

### Adjusting guestroot for MeeTTY Practice Terminal

Few changes need to be done in guestroot to make sure **Practice Terminal** looks good to users.

* Make sure **/etc/resolv.conf** available in guestroot.
* Make sure **/etc/localtime** is available and pointing to (or copied from) proper timezone under **/usr/share/zoneinfo** in guestroot.
* Make sure **/etc/resolv.conf**, **/etc/passwd**, **/etc/group**, **/etc/profile**, **/root/.bashrc** files should be writable in guestroot by the user account which you are going to use to run meetty, because meetty will do some changes to make **root** username appear as **guest_nnnn** username. This is to make sure users dont get scared because they are **root**.

## Running WeeChat In Presenter Terminal

The top pane of Presenter Termial will show current Broadcasting TMUX session. The bottom pane of Presenter Terminal is free for presenters to do whatever they want. It is better to run irc client like WeeChat so that presenters can communicate as well as type commands in the broadcasting TMUX session without switching windows. Here is how to start weechat in the Bottom pane.

```bash
$ ./meetty/scripts/weechat.sh
```

![MeeTTY weechat](../docs/screenshots/MeeTTY-weechat.png)

## Connecting to Guest's Practice Window

It is possible to connect to any guest's Practice window. First ask the guest to give Practice Terminal's session-id which will be shown in the Practice Terminal title (eg: "Practice Terminal (24561)", here 24561 is the session-id)

Once Presenter receive session-id from guest. He/She can start a new terminal window in Presenter Terminal by **pressing 'ctrl-b c' keys** and run the following command from cloned meetty directory

```bash
$ ./scripts/connect-guest.sh <guest-session-id>
(eg: scripts/connect-guest.sh 681357)
```

This will connect Presenter Terminal with guest's TMUX session. This guest's TMUX session is also run inside Presenter Terminal's TMUX session. So **you have to press tmux prefix key two times (eg: 'ctrl-b ctrl-b') to control guest's TMUX session**.

![MeeTTY guest session](../docs/screenshots/MeeTTY-guest-session.png)

To detach from guest session, **press 'ctrl-b ctrl-b d'** from the Presenter Terminal window where the guest terminal is attached.

## Broadcasting Guest's Practice Window

Presenter can broadcast guest's session. First he/she have to connect to guest's TMUX session (explained above). Then follow these steps

1. In Presenter Terminal, **press 'ctrl-b w' to view connected windows**, move up/down using up/down arrow keys to locate the window where presenter attached with guest's TMUX session (Window preview will be shown while moving up/down), you can also expand a window which contains more than 1 pane with left arrow key.

![MeeTTY widow id](../docs/screenshots/MeeTTY-window-id.png)

2. Once you locate the window where guest's TMUX session is attached, note down the window-id, for example in line '(5)  └─>   2: bash-' the number 2 before the colon (:) is the window id. So the proper window-id to locate this window is "instructor:2" where "instructor" is the session-id of Presenter Terminal.
3. Once you retrive full window-id (like "instructor:2") of the window where Presenter Terminal is connected with Guest's TMUX session, goto Presenter Terminal's first window by **pressing 'ctrl-b 0' key** and then switching to Broadcast TMUX session pane by **pressing 'ctrl-b o'** as many times needed.
4. Once you are in Broadcast TMUX session pane, link previously gathered window-id with Broadcast TMUX session by **pressing 'ctrl-b ctrl-b : link-window -s gathered-windows-id' then pressing enter key** like for example **pressing 'ctrl-b ctrl-b : link-window -s instructor:2' then pressing enter key**, the Broadcasting TMUX session will link with Presenter Terminal's window and then will start broadcasting to everyone

![MeeTTY broadcast_guest_session 0](../docs/screenshots/MeeTTY-broadcast-guest-session-0.png)

![MeeTTY broadcast_guest_session_1](../docs/screenshots/MeeTTY-broadcast-guest-session-1.png)

5. Presenter can stop broadcasting the current guest session window by **pressing 'ctrl-b ctrl-b : unlink-window' then pressing enter key**, this will detach Broadcasting TMUX session from Presenter Terminal's window where the guest session is attached.

Get Involved:
=============

* **Official Git Repo:** https://gitlab.com/mohan43u/meetty
* **Bug Reporting:** Create an issue in https://gitlab.com/mohan43u/meetty/-/issues and provide as much information as possible
* **IRC:** Developers hangout in **#ilugc** channel on [libera.chat](https://libera.chat)
