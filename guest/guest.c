/* compile: gcc -O0 -g -static -o guest guest.c */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <wait.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <sys/prctl.h>
#include <sys/capability.h>
#include <sys/mount.h>

#define SOCKPATH "/run/meetty.sock"

#define CAPLIST {CAP_SETPCAP, CAP_NET_ADMIN, CAP_SYS_RESOURCE, CAP_MKNOD}
#define CAPLIST_LEN 4

struct _g {
	uid_t uid;
	gid_t gid;
	pid_t pid;
	pid_t child;
	pid_t gchild;
	char *progname;
	char *buf;
	char *piddir;
	char *lowerdir;
	char *upperdir;
	char *rootdir;
	char *procdir;
	char *devdir;
	char *devptsdir;
	char *devshmdir;
	char *sysdir;
	char *rundir;
	char *tmpdir;
	int pipefds[2];
} g;

static int reapchild(pid_t pid) {
	int ret = 0;
	waitpid(pid, &ret, 0);
	ret = WIFSIGNALED(ret) ? WTERMSIG(ret) : WEXITSTATUS(ret);
	return ret;
}

static void pcleanup(void) {
	if(g.child > 0) {
		if(kill(g.child, SIGTERM) < 0) {
			dprintf(2, "pcleanup: not able to kill pid %d: %s\n", g.child, strerror(errno));
		}
		else {
			reapchild(g.child);
		}
		g.child = -1;
	}
	if(g.pipefds[0] > 0) {
		close(g.pipefds[0]);
		g.pipefds[0] = -1;
	}
	if(g.pipefds[1] > 0) {
		close(g.pipefds[1]);
		g.pipefds[1] = -1;
	}
}

static void sighandler_pcleanup(int sig) {
	pcleanup();
}

static int exists(const char *file) {
	return (access(file, F_OK) == 0);
}

static void makedir(const char *dir) {
	char *mkdirbuf = NULL;
	if(exists(dir) == 0) {
		asprintf(&mkdirbuf, "mkdir -p %s", dir);
		if(system(mkdirbuf) != 0) {
			dprintf(2, "failed to create %s\n", dir);
		}
		free(mkdirbuf);
		return;
	}
}

static int lmount(const char *src, const char *dst, const char *type, unsigned long flags, const char *data) {
	int ret = -1;
	if(exists(dst) != 0) {
		if((ret = mount(src, dst, type, flags, data)) < 0) {
			dprintf(2, "failed to mount %s: %s\n", src, strerror(errno));
		}
	}
	return ret;
}

static void removedir(const char *dir, int recursive) {
	char *rmdirbuf = NULL;
	if(exists(dir) != 0 && g.uid != 0) {
		asprintf(&rmdirbuf, "%s %s", recursive > 0 ? "rm -fr" : "rmdir", dir);
		if(system(rmdirbuf) != 0) {
			dprintf(2, "failed to remove %s\n", dir);
		}
		free(rmdirbuf);
		rmdirbuf = NULL;
	}
}

static void lumount(char *dir) {
	if(umount2(dir, MNT_DETACH) < 0) {
		dprintf(2, "lumount: failed to umount %s: %s\n", dir, strerror(errno));
	}
}

static void cleanup_removedir(char *dir, int recursive) {
	if(recursive) removedir(dir, recursive);
	free(dir);
}

static void cleanup_umount(char *dir, int recursive) {
	if(exists(dir) != 0) {
		lumount(dir);
		cleanup_removedir(dir, recursive);
	}
}

static void ccleanup(void) {
	if(g.gchild > 0) {
		if(kill(g.gchild, SIGTERM) < 0) {
			dprintf(2, "ccleanup: not able to kill pid %d: %s\n", g.gchild, strerror(errno));
		}
		else {
			reapchild(g.gchild);
		}
		g.gchild = -1;
	}

	if(g.pipefds[0] > 0) {
		close(g.pipefds[0]);
		g.pipefds[0] = -1;
	}
	if(g.pipefds[1] > 0) {
		close(g.pipefds[1]);
		g.pipefds[1] = -1;
	}

	if(g.buf != NULL) {
		free(g.buf);
		g.buf = NULL;
	}

	if(g.tmpdir != NULL) {
		cleanup_umount(g.tmpdir, 0);
		g.tmpdir = NULL;
	}

	if(g.rundir != NULL) {
		cleanup_umount(g.rundir, 0);
		g.rundir = NULL;
	}

	if(g.sysdir != NULL) {
		cleanup_umount(g.sysdir, 0);
		g.sysdir = NULL;
	}

	if(g.devshmdir != NULL) {
		cleanup_umount(g.devshmdir, 0);
		g.devshmdir = NULL;
	}

	if(g.devptsdir != NULL) {
		cleanup_umount(g.devptsdir, 0);
		g.devptsdir = NULL;
	}

	if(g.devdir != NULL) {
		cleanup_umount(g.devdir, 0);
		g.devdir = NULL;
	}

	if(g.procdir != NULL) {
		cleanup_umount(g.procdir, 0);
		g.procdir = NULL;
	}

	if(g.uid != 0 && g.rootdir != NULL) {
		cleanup_umount(g.rootdir, 1);
		g.rootdir = NULL;
	}

	if(g.upperdir != NULL) {
		cleanup_removedir(g.upperdir, 1);
		g.upperdir = NULL;
	}

	if(g.piddir != NULL) {
		cleanup_umount(g.piddir, 1);
		g.piddir = NULL;
	}
}

static void sighandler_ccleanup(int sig) {
	ccleanup();
}

static void handle_signals(sighandler_t handlerfunc) {
	struct sigaction term = {};
	struct sigaction intr = {};

	term.sa_handler = handlerfunc;
	intr.sa_handler = handlerfunc;
	if(sigaction(SIGTERM, &term, NULL) < 0) {
		dprintf(2, "handle_signals: failed to set handler for SIGTERM: %s\n", strerror(errno));
	}
	if(sigaction(SIGINT, &intr, NULL) < 0) {
		dprintf(2, "handle_signals: failed to set handler for SIGINT: %s\n", strerror(errno));
	}
}

static void freestrv(char **envp) {
	if(envp) {
		while(*envp) {
			if(*envp) free(*envp);
			*envp = NULL;
			envp++;
		}
	}
}

static void addstrv(int *envcp, char ***envpp, char *env) {
	char **envp = *envpp;
	envp = realloc(envp, (*envcp + 1) * sizeof(char *));
	envp[*envcp] = env;
	*envcp = *envcp + 1;
	*envpp = envp;
}

static void addstr(char **oldp, char *new) {
	char *old = *oldp;
	size_t old_len = 0;
	size_t new_len = 0;
	size_t len = 0;

	old_len = (old == NULL ? 0 : strlen(old));
	new_len = (new == NULL ? 0 : strlen(new));
	len = old_len + new_len;
	old = realloc(old, len + 1);
	strcpy(old + old_len, new);
	old[len] = '\0';
	*oldp = old;
}

static void replace_str(const char *file, char *delim, char *old_str, char *new_str) {
	struct stat s;
	int fd = -1;
	char *raddr = NULL;
	char *waddr = NULL;
	char *rfield = NULL;
	char *wfield = NULL;
	char *field = NULL;
	char *line = NULL;
	ssize_t len = 0;

	if((fd = open(file, O_RDONLY)) < 0) {
		dprintf(2, "replace_str: read: open: %s\n", strerror(errno));
		goto cleanup;
	}

	if(fstat(fd, &s) < 0) {
		dprintf(2, "replace_str: read: stat: %s\n", strerror(errno));
		goto cleanup;
	}

	if((raddr = malloc(s.st_size + 1)) == NULL) {
		dprintf(2, "replace_str: read: malloc failed\n");
		goto cleanup;
	}

	if((len = read(fd, raddr, s.st_size)) < s.st_size) {
		dprintf(2, "replace_str: read: read failed\n");
		goto cleanup;
	}
	raddr[len] = '\0';
	close(fd);

	line = raddr;
	while(line && *line) {
		rfield = line;
		line = strchr(line, '\n');
		if(line) {
			*line = '\0';
			line += 1;
		}

		while(rfield && *rfield) {
			field = rfield;
			rfield = strstr(rfield, delim);
			if(rfield) {
				*rfield = '\0';
				rfield += strlen(delim);
			}

			wfield = (strcmp(field, old_str) == 0 ? new_str : field);
			addstr(&waddr, wfield);
			addstr(&waddr, delim);
		}
		addstr(&waddr, "\n");
	}

	if((fd = open(file, O_WRONLY)) < 0) {
		dprintf(2, "replace_str: write: open: %s\n", strerror(errno));
		goto cleanup;
	}

	len = strlen(waddr);
	if(write(fd, waddr, len) < len) {
		dprintf(2, "replace_str: write: write failed\n");
	}
	close(fd);

cleanup:
	if(fd > 0) close(fd);

	if(raddr) {
		free(raddr);
		raddr = NULL;
	}

	if(waddr) {
		free(waddr);
		waddr = NULL;
	}
}

static void append_str(const char *file, const char *str) {
	int fd = -1;
	ssize_t len = -1;
	if((fd = open(file, O_WRONLY|O_APPEND|O_CREAT, S_IRWXU|S_IRGRP|S_IXGRP|S_IROTH|S_IXOTH)) < 0) {
		dprintf(2, "failed to open %s: %s\n", file, strerror(errno));
		goto cleanup;
	}
	len = strlen(str);
	if(write(fd, str, len) < len) {
		dprintf(2, "failed to write %s to %s: %s\n", str, file, strerror(errno));
		goto cleanup;
	}
cleanup:
	if(fd > 0) close(fd);
}

static void copy_file(const char *src, const char *dst) {
	int fd = -1;
	int fdw = -1;
	struct stat s;
	char *content = NULL;

	if((fd = open(src, O_RDONLY)) < 0) {
		dprintf(2, "copy_file: read: failed to open %s: %s\n", src, strerror(errno));
		goto cleanup;
	}
	if(fstat(fd, &s) < 0) {
		dprintf(2, "copy_file: stat: %s\n", strerror(errno));
		goto cleanup;
	}
	
	if((content = mmap(NULL, s.st_size, PROT_READ, MAP_PRIVATE, fd, 0)) == MAP_FAILED) {
		dprintf(2, "failed to map src %s: %s\n", src, strerror(errno));
		goto cleanup;
	}

	if((fdw = open(dst, O_WRONLY|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH)) < 0) {
		dprintf(2, "failed to open dst %s: %s\n", dst, strerror(errno));
		goto cleanup;
	}

	if(ftruncate(fdw, s.st_size) < 0) {
		dprintf(2, "failed to truncate %s: %s\n", dst, strerror(errno));
		goto cleanup;
	}

	if(write(fdw, content, s.st_size) < s.st_size) {
		dprintf(2, "failed to write to dst %s: %s\n", dst, strerror(errno));
		goto cleanup;
	}
cleanup:
	if(fd > 0) close(fd);
	if(content != NULL) {
		munmap(content, s.st_size);
		content = NULL;
	}
	if(fdw > 0) close(fdw);
}

static void drop_caps(void) {
	int caplist[CAPLIST_LEN] = CAPLIST;
	for(int i = 0; i < CAPLIST_LEN; i++) {
		if(prctl(PR_CAPBSET_DROP, caplist[i], 0, 0, 0) < 0) {
			dprintf(2, "drop_caps: failed to drop cap %d: %s\n", caplist[i], strerror(errno));
		}
	}
}

static void initialize_limit(pid_t pid) {
	int fd = 0;
	char *pidstring = NULL;
	struct sockaddr_un addr = {};
	fd_set rfds = {};
	struct timeval timeout = {
		.tv_sec = 5,
		.tv_usec = 0
	};
	int ret = -1;

	if((fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		dprintf(2, "initialize_limit: failed to create socket: %s\n", strerror(errno));
		goto cleanup;
	}

	asprintf(&pidstring, "%d", pid);
	addr.sun_family = AF_UNIX;
	strncpy(addr.sun_path, SOCKPATH, sizeof(addr.sun_path) - 1);

	if(connect(fd, &addr, sizeof(addr)) < 0) {
		dprintf(2, "initialize_limit: failed to connect to limit daemon: %s\n", strerror(errno));
		goto cleanup;
	}
	if(send(fd, pidstring, strlen(pidstring), 0) < 0) {
		dprintf(2, "initialize_limit: failed to send %s to limit daemon: %s\n", pidstring, strerror(errno));
		goto cleanup;
	}

	FD_ZERO(&rfds);
	FD_SET(fd, &rfds);
	if((ret = select(fd + 1, &rfds, NULL, NULL, &timeout)) < 0) {
		dprintf(2, "initialize_limit: failed to get response from limit daemon: %s\n", strerror(errno));
		goto cleanup;
	}
	if(ret == 0) {
		dprintf(2, "initialize_limit: timeout waiting for response from limit daemon\n");
		goto cleanup;
	}
	if(ret > 0) {
		if(recv(fd, pidstring, strlen(pidstring), 0) < 0) {
			dprintf(2, "initialize_limit: failed to recv from limit daemon: %s\n", strerror(errno));
			goto cleanup;
		}
	}

cleanup:
	if(pidstring != NULL) {
		free(pidstring);
		pidstring = NULL;
	}

	if(fd) {
		if(close(fd) < 0) {
			dprintf(2, "initialize_limit: failed to close file: %s\n", strerror(errno));
		}
		fd = 0;
	}
}

static int childfunc(int argc, char *argv[]) {
	int ret = 0;
	char *default_cmdline[] = { "/bin/sh", "-l", NULL };

	atexit(ccleanup);
	handle_signals(sighandler_ccleanup);

	if(argc <= 0) {
		dprintf(2, "[usage] %s <chroot-path> [cmd [args..]]\n", g.progname);
		ret = 1;
		goto cleanup;
	}
	g.lowerdir = argv[0];
	argc--;
	argv++;

	if(g.uid != 0) {
		initialize_limit(g.child);

		asprintf(&g.piddir, "/tmp/meetty/%d", g.pid);
		makedir(g.piddir);
		if(lmount("tmpfs", g.piddir, "tmpfs", 0, NULL) < 0) {
			dprintf(1, "piddir: mount failed\n");
			ret = 1;
			goto cleanup;
		}
		asprintf(&g.upperdir, "%s/upper", g.piddir);
		makedir(g.upperdir);
		asprintf(&g.rootdir, "%s/root", g.piddir);
		makedir(g.rootdir);
		asprintf(&g.buf, "${fuseoverlayfsexec:-fuse-overlayfs} -o lowerdir=%s,upperdir=%s,workdir=%s %s", g.lowerdir, g.upperdir, g.piddir, g.rootdir);
		if(system(g.buf) != 0) {
			dprintf(1, "mount %s command failed\n", g.rootdir);
			ret = 1;
			goto cleanup;
		}
		free(g.buf);
		g.buf = NULL;
	}
	else {
		g.rootdir = strdup(g.lowerdir);
	}

	asprintf(&g.procdir, "%s/proc", g.rootdir);
	makedir(g.procdir);
	if(lmount("proc", g.procdir, "proc", 0, NULL) < 0) {
		dprintf(1, "proc: mount failed\n");
		ret = 1;
		goto cleanup;
	}

	asprintf(&g.devdir, "%s/dev", g.rootdir);
	makedir(g.devdir);
	if(g.uid != 0) {
		if(lmount("/dev", g.devdir, NULL, MS_BIND|MS_REC, NULL) < 0) {
			dprintf(1, "devtmpfs: mount failed\n");
			ret = 1;
			goto cleanup;
		}
	}
	else {
		if(lmount("devtmpfs", g.devdir, "devtmpfs", 0, NULL) < 0) {
			dprintf(1, "devtmpfs: mount failed\n");
			ret = 1;
			goto cleanup;
		}
	}

	asprintf(&g.devptsdir, "%s/dev/pts", g.rootdir);
	makedir(g.devptsdir);
	if(lmount("devpts", g.devptsdir, "devpts", 0, "mode=620,ptmxmode=0666") < 0) {
		dprintf(1, "devpts: mount failed\n");
		ret = 1;
		goto cleanup;
	}

	asprintf(&g.devshmdir, "%s/dev/shm", g.rootdir);
	makedir(g.devshmdir);
	if(lmount("devshm", g.devshmdir, "tmpfs", 0, NULL) < 0) {
		dprintf(1, "devshm: mount failed\n");
		ret = 1;
		goto cleanup;
	}

	asprintf(&g.sysdir, "%s/sys", g.rootdir);
	makedir(g.sysdir);
	if(g.uid != 0) {
		if(lmount("sysfs", g.sysdir, "tmpfs", 0, NULL) < 0) {
			dprintf(1, "sysfs: mount failed\n");
			ret = 1;
			goto cleanup;
		}
	}
	else {
		if(lmount("sysfs", g.sysdir, "sysfs", 0, NULL) < 0) {
			dprintf(1, "sysfs: mount failed\n");
			ret = 1;
			goto cleanup;
		}
	}

	asprintf(&g.rundir, "%s/run", g.rootdir);
	makedir(g.rundir);
	if(lmount("run", g.rundir, "tmpfs", 0, NULL) < 0) {
		dprintf(1, "run: mount failed\n");
		ret = 1;
		goto cleanup;
	}

	asprintf(&g.tmpdir, "%s/tmp", g.rootdir);
	makedir(g.tmpdir);
	if(lmount("tmp", g.tmpdir, "tmpfs", 0, NULL) < 0) {
		dprintf(1, "tmp: mount failed\n");
		ret = 1;
		goto cleanup;
	}

	if(g.uid != 0) {
		drop_caps();
	}

	if(argc <= 0) {
		argv = default_cmdline;
	}

	if((g.gchild = fork()) < 0) {
		dprintf(2, "failed to create child: %s\n", strerror(errno));
		ret = 1;
		goto cleanup;
	}
	else if(g.gchild == 0) {
		int vc = 0;
		char **v = NULL;
		char *env = NULL;

		if(g.uid != 0) {
			asprintf(&g.buf, "%s/etc/localtime", g.rootdir);
			if(access(g.buf, F_OK) < 0) {
				copy_file("/etc/localtime", g.buf);
			}
			free(g.buf);
			g.buf = NULL;
		}

		if(chroot(g.rootdir) < 0) {
			dprintf(2, "chroot: not able to chroot into %s: %s\n", g.rootdir, strerror(errno));
			ret = 1;
			goto cleanup_gchild;
		}

		if(g.uid != 0) {
			truncate("/etc/resolv.conf", 0);
			append_str("/etc/resolv.conf", "nameserver 1.1.1.1\n");
			append_str("/root/.profile", "\nexport PS1=\"[\\u@\\h: \\W]$ \"\n");
			append_str("/root/.bashrc", "\nexport PS1=\"[\\u@\\h: \\W]$ \"\n");
			asprintf(&g.buf, "guest_%d", g.pid);
			replace_str("/etc/passwd", ":", "root", g.buf);
			replace_str("/etc/group", ":", "root", g.buf);
			free(g.buf);
			g.buf = NULL;
		}

		addstrv(&vc, &v, strdup("HOME=/root"));
		addstrv(&vc, &v, strdup("UID=0"));
		addstrv(&vc, &v, strdup("EUID=0"));
		addstrv(&vc, &v, strdup("GID=0"));
		addstrv(&vc, &v, strdup("EGID=0"));
		addstrv(&vc, &v, strdup("SHELL=/bin/bash"));
		asprintf(&env, "USER=guest_%d", g.pid);
		addstrv(&vc, &v, env);
		env = NULL;
		asprintf(&env, "LOGNAME=guest_%d", g.pid);
		addstrv(&vc, &v, env);
		env = NULL;
		asprintf(&env, "TERM=%s", getenv("TERM"));
		addstrv(&vc, &v, env);
		env = NULL;
		asprintf(&env, "LANG=%s", getenv("LANG"));
		addstrv(&vc, &v, env);
		env = NULL;
		addstrv(&vc, &v, NULL);

		if(chdir("/root") < 0) {
			dprintf(2, "chdir: %s\n", strerror(errno));
		}

		if(execvpe(argv[0], argv, v) < 0) {
			dprintf(2, "child_func: execvpe failed: %s\n", strerror(errno));
			ret = 1;
			goto cleanup_gchild;
		}
	cleanup_gchild:
		if(v != NULL) {
			freestrv(v);
			v = NULL;
		}
		exit(ret);
	}
	else {
		ret = reapchild(g.gchild);
		g.child = -1;
		g.gchild = -1;
	}
cleanup:
	return ret;
}

int main(int argc, char *argv[]) {
	int ret = 0;
	int fd = -1;
	char *data = NULL;

	atexit(pcleanup);
	handle_signals(sighandler_pcleanup);

	g.progname = argv[0];
	g.pid = getpid();
	g.uid = geteuid();
	g.gid = getegid();

	if(g.uid != 0) {
		if(unshare(CLONE_NEWUSER|CLONE_NEWNS|CLONE_NEWPID|CLONE_NEWCGROUP|CLONE_NEWNET) < 0) {
			dprintf(2, "unshare: failed to unshare: %s\n", strerror(errno));
		}

		if((fd = open("/proc/self/uid_map", O_WRONLY)) < 0) {
			dprintf(2, "uid_map: open: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		asprintf(&data, "0 %u %d", g.uid, g.uid == 0 ? 65535 : 1);
		if((write(fd, data, strlen(data))) < (ssize_t)strlen(data)) {
			dprintf(2, "uid_map: write: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		free(data);
		data = NULL;
		if(close(fd) < 0) {
			dprintf(2, "uid_map: closing %d failed: %s\n", fd, strerror(errno));
			ret = 1;
			goto cleanup;
		}
		fd = -1;

		if((fd = open("/proc/self/setgroups", O_WRONLY)) < 0) {
			dprintf(2, "setgroups: open: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		asprintf(&data, "deny");
		if((write(fd, data, strlen(data))) < (ssize_t)strlen(data)) {
			dprintf(2, "setgroups: write: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		free(data);
		data = NULL;
		if(close(fd) < 0) {
			dprintf(2, "setgroups: closing %d failed: %s\n", fd, strerror(errno));
			ret = 1;
			goto cleanup;
		}
		fd = -1;

		if((fd = open("/proc/self/gid_map", O_WRONLY)) < 0) {
			dprintf(2, "gid_map: open: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		asprintf(&data, "0 %u %d", g.gid, g.gid == 0 ? 65535 : 1);
		if((write(fd, data, strlen(data))) < (ssize_t)strlen(data)) {
			dprintf(2, "gid_map: write: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		free(data);
		data = NULL;
		if(close(fd) < 0) {
			dprintf(2, "gid_map: closing %d failed: %s\n", fd, strerror(errno));
			ret = 1;
			goto cleanup;
		}
		fd = -1;
	}

	if(pipe(g.pipefds) < 0) {
		dprintf(2, "pipe:  failed to create pipe: %s\n", strerror(errno));
		ret = 1;
		goto cleanup;
	}

	if((g.child = fork()) < 0) {
		dprintf(2, "fork: failed to create child: %s\n", strerror(errno));
		ret = 1;
		goto cleanup;
	}
	else if(g.child == 0) {
		close(g.pipefds[1]);
		g.pipefds[1] = -1;
		if(read(g.pipefds[0], &g.child, sizeof(g.child)) < (ssize_t) sizeof(g.child)) {
			dprintf(2, "pipe: failed to receive g.child from parent: %s\n", strerror(errno));
			exit(1);
		}
		close(g.pipefds[0]);
		g.pipefds[0] = -1;

		argc--;
		argv++;
		exit(childfunc(argc, argv));
	}
	else {
		close(g.pipefds[0]);
		g.pipefds[0] = -1;
		if(write(g.pipefds[1], &g.child, sizeof(g.child)) < (ssize_t) sizeof(g.child)) {
			dprintf(2, "pipe: failed to send g.child to child: %s\n", strerror(errno));
			ret = 1;
			goto cleanup;
		}
		close(g.pipefds[1]);
		g.pipefds[1] = -1;

		ret = reapchild(g.child);
		g.child = -1;
	}

cleanup:
	if(data != NULL) {
		free(data);
		data = NULL;
	}
	if(fd > 0) {
		if(close(fd) < 0) {
			dprintf(2, "cleanup: closing %d failed: %s\n", fd, strerror(errno));
		}
		fd = -1;
	}
	return ret;
}
