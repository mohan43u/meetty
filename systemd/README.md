This directory contains example systemd service files

### MeeTTY Limit Service

This systemd service will provide resource limitation to guest terminals. This needs to be run as root. To Configure meetty-limit.service, follow these steps,

* copy [meetty-limit.service](./meetty-limit.service) file to /etc/systemd/system directory
* As root user, open /etc/systemd/system/meetty-limit.service file
* Uncomment **ExecStart** line and replace **/path/to/meetty/git/cloned/directory** to proper place where **meetty** git directory was cloned

### MeeTTY Service

This is the primary systemd service to run MeeTTY. To Configure meetty.service, follow these steps,

* copy [meetty.service](./meetty.service) file to /etc/systemd/system directory
* As root user, open /etc/systemd/system/meetty.service file
* Uncomment **User=** line and replace **username** to the user account you prefer to run meetty service. meetty will run under this username.
* Uncomment **ExecStart** line replace **/path/to/meetty/git/cloned/directory** to proper place where **meetty** git directory was cloned, make sure this is accessable by the user account you configured in **User=** field.
* Replace **host-or-ip** to your prefered hostname or ip address
* Replace **port** to your prefered port. make sure the port number you give here as well as the next port number is reachable (ie: 8000 and 8001) by your users.
* Run the following commands to enable and start meetty as well as meetty-limit

```bash
$ sudo systemctl daemon-reload
$ sudo systemctl enable meetty.service
$ sudo systemctl start meetty.service
```
