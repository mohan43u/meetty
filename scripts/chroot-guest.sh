#!/bin/bash
usage="[usage] ${0} [guestroot] [cmd [args..]]"
[[ "${1}" == "--help" ]] && { echo "${usage}" 1>&2; exit 0; }

me=$(readlink -f "${0}")
basedir=$(readlink -f $(dirname "${me}")/..)
guestroot="${1:-default}"
shift
if [[ "${guestroot}" == "default" ]]; then
    guestroot="${basedir}/guest/root"
fi

export fuseoverlayfsexec="${basedir}"/fuse-overlayfs/fuse-overlayfs
exec "${basedir}"/guest/guest "${guestroot}" "${@}"
