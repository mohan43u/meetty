#!/bin/bash

me=$(readlink -f "${0}")
myname=$(basename "${me}")

record() {
    typescriptdir="${HOME}/.${myname}/"
    [[ ! -d "${typescriptdir}" ]] && mkdir -p "${typescriptdir}"
    timestamp=$(date +'%Y%m%d%H%M%S')
    typescriptname="${typescriptdir}/${timestamp}-${$}"
    typescripttiming="${typescriptname}.timing"
    typescript="${typescriptname}.typescript"
    if [[ "${#}" -gt 0 ]]; then
	script --timing="${typescripttiming}" --command "${*}" "${typescript}"
    else
	script --timing="${typescripttiming}" "${typescript}"
    fi
}
record "${*}"
