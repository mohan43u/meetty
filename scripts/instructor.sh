#!/bin/bash

cleanup() {
    if [[ "${stage}" -eq 1 ]]; then
	if [[ -n "${root}" ]]; then
	    umount "${meettypath}"/guest/root
	    rmdir "${meettypath}"/guest/root || { echo "failed to remove old root" 1>&2; return; }
	    if [[ -e "${meettypath}"/guest/root.orig ]]; then
		mv "${meettypath}"/guest/root.orig "${meettypath}"/guest/root
	    fi
	fi
    fi
}
trap cleanup EXIT

get_stage() {
    ppid=$(grep '^PPid' /proc/"${$}"/status | awk '{print $2}')
    pcmd=$(tr '\0' '\n' < /proc/${ppid}/cmdline | head -1)
    if [[ "${pcmd}" =~ ssh ]]; then
	echo 1
	return
    fi
    echo 0
}

usage="[usage] $0 <remote-meettypath> <ssh-ip> <ssh-username> [guestroot] [ssh-port] [ssh-rtunnel-port] [ssh-rtunnel-local-ip] [ssh-rtunnel-local-port] [ssh-rtunnel-username]"
if [[ "${#}" -lt 3 ]]; then
    echo "${usage}"
    exit 1
fi

myname=$(basename $(readlink -f "${0}"))
export stage=$(get_stage)
meettypath="${1}"
server="${2}"
user="${3}"
root="${4}"
port="${5:-22}"
export rport="${6:-2222}"
rlocalip="${7:-127.0.0.1}"
rlocalport="${8:-22}"
export ruser="${9:-${USER}}"
rtunnel="${rport}:${rlocalip}:${rlocalport}"

if [[ "${stage}" -eq 1 ]]; then
    rm "${HOME}"/.ssh/known_hosts
    if [[ -n "${root}" ]]; then
	if [[ -e "${meettypath}"/guest/root ]]; then
	    mv "${meettypath}"/guest/root "${meettypath}"/guest/root.orig
	fi
	mkdir "${meettypath}"/guest/root || { echo "cannot create ${meettypath}/guest/root" 1>&2; exit 1; }
	GID=$(getent passwd "${UID}" | cut -d':' -f3)
	sshfs -p 2222 -o uid=${UID} -o gid=${GID} 127.0.0.1:"${root}" "${meettypath}"/guest/root || { echo "cannot mount ${root}" 1>&2; exit 1; }
    fi
    "${meettypath}"/scripts/tmux-share.sh
    exit
fi

if [[ "$(ssh -p ${rlocalport} ${ruser}@${rlocalip} 'echo test')" != "test" ]]; then
    echo "you dont have permission to login to ${ruser}@${rlocalip}"
    exit 1
fi

ssh -o ExitOnForwardFailure=yes -t -p "${port}" -R "${rtunnel}" "${user}@${server}" "${meettypath}/scripts/${myname} ${meettypath} ${server} ${user} '${root}' ${port} ${rport} ${rlocalip} ${rlocalport} ${ruser}"
