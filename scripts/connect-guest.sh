#!/bin/bash
usage="[usage] $0 <guest-session-id>"
([[ "${#}" -lt 1 ]] || [[ "${1}" == "--help" ]]) && { echo "${usage}" 1>&2; exit 1; }

me=$(readlink -f "${0}")
basedir=$(readlink -f $(dirname "${me}")/..)
pid="${1}"

childpid=$(pstree -aAp "${pid}" | sed '1d' | head -1 | cut -d',' -f2 | cut -d' ' -f1)
unset TMUX
nsenter -m -p -U --preserve-credentials -t "${childpid}" chroot /tmp/meetty/"${pid}"/root /usr/bin/tmux -2 -u -f /dev/null attach-session -t 0
