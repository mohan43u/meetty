#!/bin/bash

if [[ -z "${TERM}" ]] || [[ "${TERM}" == "dumb" ]]; then
    export TERM="xterm-256color"
fi

usage="[usage] $0 <ip> <port>"
[[ "${#}" -lt 2 ]] && { echo "${usage}" 1>&2; exit 1; }

me=$(readlink -f "${0}")
basedir=$(readlink -f $(dirname "${me}")/..)

if [[ ! -e "/dev/fuse" ]] || [[ "$(stat=$(stat -c '%A' /dev/fuse); echo ${stat: -3})" != "rw-" ]]; then
    echo "/dev/fuse not available or not accessable." 1>&1;
    exit 1
fi
export fuseoverlayfsexec="${basedir}"/fuse-overlayfs/fuse-overlayfs
if [[ ! -x "${fuseoverlayfsexec}" ]]; then
    echo "cannot find fuse-overlayfs binary (or not executable)" 1>&2
    exit 1
fi

tmuxexec="${basedir}"/tmux/tmux
if [[ ! -x "${tmuxexec}" ]]; then
    echo "cannot find tmux binary (or not executable)" 1>&2
    exit 1
fi

guestroot="${guestroot:-${basedir}/guest/root}"
guestroot=$(realpath -se "${guestroot}")
if [[ ! -d "${guestroot}" ]] && [[ ! -h "${guestroot}" ]] ; then
    echo "guestroot is not available (required for practice terminal)" 1>&2
    exit 1;
fi

guestinitprog="${guestinitprog:-/usr/bin/tmux}"
guestinitargs="${guestinitargs:- -2 -u -f /dev/null}"
if ! (stat "${guestroot}/${guestinitprog}" >/dev/null); then
    if [[ $(basename "${guestinitprog}") == "tmux" ]]; then
	install -D "${tmuxexec}" "${guestroot}/${guestinitprog}"
    else
	echo "cannot find ${guestinitprog} binary (or not executable) in ${guestroot}" 1>&2
	exit 1;
    fi
fi

export tmuxtest="exec ${tmuxexec} -2 -u -f /dev/null -L share has-session -t broadcast"
export tmuxalt="exec dialog --title 'Welcome to MeeTTY' --infobox 'Presenter yet to come, stay tuned.' 3 40"
export tmuxcmdline="exec ${tmuxexec} -2 -u -f /dev/null -L share attach-session -f read-only -t broadcast"
export nspawncmdline="exec ${basedir}/guest/guest ${guestroot} ${guestinitprog} ${guestinitargs}"

"${basedir}"/scripts/bootstrap poetry run python -m src.meetty.app "${@}"
