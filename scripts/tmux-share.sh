#!/bin/bash

instructor_clients_num() {
    "${tmuxexec}" -2 -u -f /dev/null -L share list-clients -t instructor 2>/dev/null | wc -l
}

cleanup() {
    if [[ "$(instructor_clients_num)" -le 0 ]]; then
	"${tmuxexec}" -2 -u -f /dev/null -L share kill-session -t broadcast
	${basedir}/scripts/control-guest.sh disablenetwork
	for node in ${nspawnnodes}
	do
	    ssh -n "${node}" "${basedir}/scripts/control-guest.sh disablenetwork"
	done
    fi
    rm -f "${tempfile0}"
}
trap cleanup EXIT

me=$(readlink -f "${0}")
myname=$(basename "${me}")
basedir=$(readlink -f $(dirname "${me}")/..)

export tmuxexec="${basedir}"/tmux/tmux
if [[ ! -x "${tmuxexec}" ]]; then
    echo "cannot proceed without tmux>=3.1b-2f89d2e7" 1>&2
    exit 1
fi

tempfile0=$(mktemp -t "${myname}".XXXXXX)
tmux_share() {
    tmuxshareexec="${tmuxexec} -2 -u -f /dev/null -L share attach-session -f read-only -t instructor"

    if [[ "$(instructor_clients_num)" -le 0 ]]; then
	if [[ "${stage}" -eq 1 ]]; then
	    export LANG=$(locale -a | grep 'utf8$' | grep '^en' | head -1)
	    "${tmuxexec}" -2 -u -f /dev/null -L share new-session -A -s broadcast ssh -p "${rport}" "${ruser}"@127.0.0.1 \; detach-client
	fi

	cat >"${tempfile0}" <<EOF
set -g default-terminal tmux-256color
set -gw window-size smallest
split-window -b -t instructor:0 'unset TMUX; ${tmuxexec} -2 -u -f /dev/null -L share new-session -A -s broadcast'
resize-pane -t instructor:0.1 -D 7
EOF

	${basedir}/scripts/control-guest.sh enablenetwork
	for node in ${nspawnnodes}
	do
	    ssh -n "${node}" "${basedir}/scripts/control-guest.sh enablenetwork"
	done
	tmuxshareexec="${tmuxexec} -2 -u -f /dev/null -L share new-session -A -s instructor \; source-file ${tempfile0}"
    fi

    if [[ "${record}" -ge 1 ]]; then
	${basedir}/scripts/record.sh "${tmuxshareexec}"
    else
	eval "${tmuxshareexec}"
    fi
}
tmux_share
