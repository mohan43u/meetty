#!/bin/bash
usage="[usage] ${0} <disablenetwork|enablenetwork>"
command="${1}"
if [[ "${#}" -lt 1 ]] || [[ "${command}" == "-h" ]] || [[ "${command}" == "--help" ]]; then
    echo "${usage}" 1>&2
    exit 1
fi
nccommand=$(which nc)
if [[ -z "${nccommand}" ]]; then
    echo "install nc command" 1>&2
    exit 1
fi
echo "${command}" | "${nccommand}" -U /run/meetty.sock >/dev/null
