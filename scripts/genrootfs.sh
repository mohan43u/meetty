#!/bin/bash

cleanup() {
    cd "${currentdir}"
}
trap cleanup EXIT

usage="[usage] ${0} registryimagename [guestroot]"
if [[ "${#}" -lt 1 ]]; then
    echo "${usage}" 1>&2
    exit 1
fi

me=$(readlink -f "${0}")
basedir=$(readlink -f $(dirname "${me}")/..)
registryimagename="${1}"
guestroot="${2:-default}"
if [[ "${guestroot}" == "default" ]]; then
    guestroot="${basedir}/guest/root"
fi
guestrootoci="${guestroot}oci"
guestrootraw="${guestroot}raw"

skopeo=$(type -P skopeo)
if [[ -z "${skopeo}" ]]; then
    echo "skopeo command not available" 1>&2
    exit 1
fi
umoci=$(type -P umoci || echo "${basedir}/umoci/umoci")

skopeo copy "${registryimagename}" oci:"${guestrootoci}":latest
${umoci} unpack --rootless --image "${guestrootoci}":latest "${guestrootraw}"
mv "${guestrootraw}/rootfs" "${guestroot}"
rm -fr "${guestrootoci}" "${guestrootraw}"
