/* compile: gcc -O0 -g -static -pthread -o limit limit.c */

#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/socket.h>
#include <poll.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <byteswap.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/capability.h>
#include <pthread.h>

#define BRIDGE "meettybr0"
#define BRIDGE_IP "172.24.0.1"
#define SUFFIX "13"
#define BRIDGE_ADDR BRIDGE_IP"/"SUFFIX
#define ADDR_MIN 2887254018
#define ADDR_MAX 2887778302
#define GUEST_CGROUP_BASE "/run/meetty/"
#define SOCKPATH "/run/meetty.sock"
#define GUEST_CGROUP_PIDS GUEST_CGROUP_BASE"pids"
#define GUEST_CGROUP_MEMORY GUEST_CGROUP_BASE"memory"
#define GUEST_CGROUP_CPU GUEST_CGROUP_BASE"cpu"
#define DEFAULT_MAX_PIDS "50"
#define DEFAULT_MAX_MEM "100m"
#define DEFAULT_MAX_FD "1024"
#define DEFAULT_CPU_PERIOD "100000"
#define DEFAULT_CPU_QUOTA "10000"
#define DEFAULT_TC_BANDWIDTH "1mbit"

struct globals {
	int fd;
	char *buf;
	char *usage;
	char *data;
	char *max_pids;
	char *max_mem;
	char *cpu_period;
	char *cpu_quota;
	char *tc_bandwidth;
	char *guestaddr;
	char *maxfd;
} g;

static void guest_cgroup_killall(const char *cgroup) {
	if(access(cgroup, R_OK|W_OK|X_OK) == 0) {
		asprintf(&g.buf, "(for d in %s/meetty/[0-9]*; do xargs kill -9 < $d/cgroup.procs; done) 2>/dev/null", cgroup);
		system(g.buf);
		free(g.buf);
		g.buf = NULL;
	
		asprintf(&g.buf, "(for d in %s/meetty/[0-9]*; do rmdir $d; done) 2>/dev/null", cgroup);
		system(g.buf);
		free(g.buf);
		g.buf = NULL;

		asprintf(&g.buf, "rmdir %s/meetty", cgroup);
		system(g.buf);
		free(g.buf);
		g.buf = NULL;
	}
}

static void guest_cgroup_rmdir(const char *dir) {
	if(access(dir, R_OK|W_OK|X_OK) == 0) {
		asprintf(&g.buf, "rmdir %s\n", dir);
		if(system(g.buf) != 0) {
			dprintf(2, "guest_cgroup_rmdir: failed to remove %s: %s\n", dir, strerror(errno));
		}
		free(g.buf);
		g.buf = NULL;
	}
}

static void guest_cgroup_umount(const char *dir) {
	if(access(dir, R_OK|W_OK|X_OK) == 0) {
		asprintf(&g.buf, "umount %s", dir);
		if(system(g.buf) != 0) {
			dprintf(2, "guest_cgroup_umount: failed to unmount %s: %s\n", dir, strerror(errno));
			_exit(1);
		}
		free(g.buf);
		g.buf = NULL;
		guest_cgroup_rmdir(dir);
	}
}

static void guest_cgroup_deinit(void) {
	guest_cgroup_killall(GUEST_CGROUP_PIDS);
	guest_cgroup_umount(GUEST_CGROUP_PIDS);
	guest_cgroup_rmdir(GUEST_CGROUP_PIDS);

	guest_cgroup_killall(GUEST_CGROUP_MEMORY);
	guest_cgroup_umount(GUEST_CGROUP_MEMORY);
	guest_cgroup_rmdir(GUEST_CGROUP_MEMORY);

	guest_cgroup_killall(GUEST_CGROUP_CPU);
	guest_cgroup_umount(GUEST_CGROUP_CPU);
	guest_cgroup_rmdir(GUEST_CGROUP_CPU);

	guest_cgroup_rmdir(GUEST_CGROUP_BASE);
}

static void guest_disable_network() {
	asprintf(&g.buf, "iptables -t nat -D POSTROUTING -s %s -j MASQUERADE 2>/dev/null", BRIDGE_ADDR);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;
}

static void guest_network_deinit(void) {
	guest_disable_network();
	asprintf(&g.buf, "ip link del %s", BRIDGE);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;
}

static void guest_cleanup(void) {
	if(g.usage != NULL) {
		free(g.usage);
		g.usage = NULL;
	}

	if(g.buf != NULL) {
		free(g.buf);
		g.buf = NULL;
	}

	if(g.data != NULL) {
		free(g.data);
		g.data = NULL;
	}

	if(g.max_pids != NULL) {
		free(g.max_pids);
		g.max_pids = NULL;
	}

	if(g.max_mem != NULL) {
		free(g.max_mem);
		g.max_mem = NULL;
	}

	if(g.cpu_period != NULL) {
		free(g.cpu_period);
		g.cpu_period = NULL;
	}

	if(g.cpu_quota != NULL) {
		free(g.cpu_quota);
		g.cpu_quota = NULL;
	}

	if(g.tc_bandwidth != NULL) {
		free(g.tc_bandwidth);
		g.tc_bandwidth = NULL;
	}

	if(g.guestaddr != NULL) {
		free(g.guestaddr);
		g.guestaddr = NULL;
	}

	if(g.maxfd != NULL) {
		free(g.maxfd);
		g.maxfd = NULL;
	}

	if(g.fd > 0) {
		if(close(g.fd) < 0) {
			dprintf(2, "cleanup: failed to close sockfd properly: %s\n", strerror(errno));
		}
		g.fd = 0;
	}
	
	guest_cgroup_deinit();
	guest_network_deinit();
	unlink(SOCKPATH);
	_exit(0);
}

static void guest_cgroup_mkdir(const char *dir) {
	if(access(dir, R_OK|W_OK|X_OK) != 0) {
		asprintf(&g.buf, "mkdir -p %s", dir);
		if(system(g.buf) != 0) {
			dprintf(2, "guest_cgroup_mkdir: failed to create %s: %s\n", dir, strerror(errno));
		}
		free(g.buf);
		g.buf = NULL;
	}
}

static void guest_cgroup_write(char *file, char *data) {
	int fd;
	size_t len = 0;

	g.buf = file;
	g.data = data;
	len = strlen(data);

	if(g.data == NULL || data[0] == '\0' || len <= 0) {
		dprintf(2, "guest_cgroup_write: invalid data to write\n");
		goto cleanup;
	}

	if(g.buf == NULL || g.buf[0] == '\0') {
		dprintf(2, "guest_cgroup_write: invalid file to write\n");
		goto cleanup;
	}
	if((fd = open(g.buf, O_WRONLY)) < 0) {
		dprintf(2, "guest_cgroup_write: cannot open %s: %s\n", g.buf, strerror(errno));
		goto cleanup;
	}
	free(g.buf);
	g.buf = NULL;

	if(write(fd, g.data, len) < len) {
		dprintf(2, "guest_cgroup_write: cannot write data to %s: %s\n", file, strerror(errno));
		goto cleanup;
	}
	free(g.data);
	g.data = NULL;

cleanup:
	if(fd > 0) {
		close(fd);
	}
	if(g.buf != NULL) {
		free(g.buf);
		g.buf = NULL;
	}
	if(g.data != NULL) {
		free(g.data);
		g.data = NULL;
	}
}

static char* get_procsfile(const char *cgroup) {
	char *procsfile = NULL;
	asprintf(&procsfile, "%s/cgroup.procs", cgroup);
	return procsfile;
}

static void guest_cgroup_add(pid_t pid) {
	char *path = NULL;
	char *file = NULL;
	char *guestpid = NULL;

	asprintf(&guestpid, "%d", pid);

	asprintf(&path, "%s/meetty/%s", GUEST_CGROUP_PIDS, guestpid);
	guest_cgroup_mkdir(path);
	asprintf(&file, "%s/pids.max", path);
	guest_cgroup_write(file, strdup(g.max_pids));
	file = NULL;
	guest_cgroup_write(get_procsfile(path), strdup(guestpid));
	free(path);
	path = NULL;

	asprintf(&path, "%s/meetty/%s", GUEST_CGROUP_MEMORY, guestpid);
	guest_cgroup_mkdir(path);
	asprintf(&file, "%s/memory.limit_in_bytes", path);
	guest_cgroup_write(file, strdup(g.max_mem));
	file = NULL;
	asprintf(&file, "%s/memory.memsw.limit_in_bytes", path);
	guest_cgroup_write(file, strdup(g.max_mem));
	file = NULL;
	guest_cgroup_write(get_procsfile(path), strdup(guestpid));
	free(path);
	path = NULL;

	asprintf(&path, "%s/meetty/%s", GUEST_CGROUP_CPU, guestpid);
	guest_cgroup_mkdir(path);
	asprintf(&file, "%s/cpu.cfs_period_us", path);
	guest_cgroup_write(file, strdup(g.cpu_period));
	file = NULL;
	asprintf(&file, "%s/cpu.cfs_quota_us", path);
	guest_cgroup_write(file, strdup(g.cpu_quota));
	file = NULL;
	guest_cgroup_write(get_procsfile(path), strdup(guestpid));
	free(path);
	path = NULL;

	free(guestpid);
}

static char* guest_get_network_addr(void) {
	char *ip = NULL;
	char *address = NULL;
	static uint available_address = ADDR_MIN;
	if(available_address < ADDR_MAX) {
		struct in_addr addr = {
			.s_addr = bswap_32(available_address++)
		};
		ip = inet_ntoa(addr);
		asprintf(&address, "%s/%s", ip, SUFFIX);
		return address;
	}
	return NULL;
}

static void guest_network_add(pid_t pid) {
	static uint index = 0;
	uint meettyl = 0;
	uint meettyr = 0;
	char *guestpid = NULL;

	asprintf(&guestpid, "%d", pid);

	g.guestaddr = guest_get_network_addr();
	if(g.guestaddr == NULL) {
		dprintf(2, "not able to obtain guestaddr\n");
		goto cleanup;
	}

	meettyl = index++;
	meettyr = index++;

	asprintf(&g.buf, "ip link add meetty%u type veth peer meetty%u netns %s", meettyl, meettyr, guestpid);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

	asprintf(&g.buf, "ip link set meetty%u master %s", meettyl, BRIDGE);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

	asprintf(&g.buf, "ip link set meetty%u up", meettyl);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

	asprintf(&g.buf, "nsenter -n -t %s ip addr add %s dev meetty%u", guestpid, g.guestaddr, meettyr);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

	asprintf(&g.buf, "nsenter -n -t %s ip link set meetty%u up", guestpid, meettyr);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

	asprintf(&g.buf, "nsenter -n -t %s ip route add default via %s", guestpid, BRIDGE_IP);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

	asprintf(&g.buf, "nsenter -n -t %s tc qdisc add dev meetty%u root handle 10: tbf rate 1mbit burst 1540 latency 100ms", guestpid, meettyr);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;

cleanup:
	if(guestpid != NULL) {
		free(guestpid);
		guestpid = NULL;
	}

	if(g.guestaddr != NULL) {
		free(g.guestaddr);
		g.guestaddr = NULL;
	}
}

static void guest_rlimit(pid_t pid) {
	ulong maxfd = 0;
	struct rlimit limit = {};

	if(strncmp(g.maxfd, "unlimited", 9) == 0) {
		maxfd = RLIM_INFINITY;
	}
	else {
		sscanf(g.maxfd, "%lu", &maxfd);
	}
	if(maxfd > 0) {
		memset(&limit, 0, sizeof(limit));
		limit.rlim_cur = maxfd;
		limit.rlim_max = maxfd;
		if(prlimit(pid, RLIMIT_NOFILE, &limit, NULL) < 0) {
			dprintf(2, "not able to set maximum open file descriptors for %d: %s\n", pid, strerror(errno));
		}
	}
}

static void guest_enable_network() {
	guest_disable_network();
	asprintf(&g.buf, "iptables -t nat -A POSTROUTING -s %s -j MASQUERADE", BRIDGE_ADDR);
	system(g.buf);
	free(g.buf);
	g.buf = NULL;
}

static void guest_reply(int fd, char *response) {
	if(send(fd, response, strlen(response), 0) < 0) {
		dprintf(2, "sendto failed: %s\n", strerror(errno));
	}
}

static int guest_compare_namespace(pid_t pid) {
	char guest_net_ns[512] = {};
	char my_net_ns[512] = {};
	char *filename = NULL;
	int ret = -1;

	asprintf(&filename, "/proc/%d/ns/net", pid);
	if(readlink(filename, guest_net_ns, 512) < 0) {
		dprintf(2, "failed to get network namespace of %d\n", pid);
		goto cleanup;
	}
	free(filename);
	filename = NULL;

	if(readlink("/proc/self/ns/net", my_net_ns, 512) < 0) {
		dprintf(2, "failed to get my network namespace\n");
		goto cleanup;
	}

	ret = (strncmp(guest_net_ns, my_net_ns, 512) == 0);
cleanup:
	if(filename != NULL) {
		free(filename);
		filename = NULL;
	}
	return ret;
}

static int handle_request(int fd) {
	char *readbuf = NULL;
	ssize_t readbuf_len = 0;

	while(1) {
		int ret = 0;
		readbuf = realloc(readbuf, readbuf_len + 512);
		if(readbuf == NULL) {
			dprintf(2, "failed to allocate buffer: %s\n", strerror(errno));
			goto cleanup;
		}
		memset(readbuf + readbuf_len, 0, 512);
		ret = recv(fd, readbuf + readbuf_len, 512, 0);
		if(ret < 0) {
			dprintf(2, "read failed: %s\n", strerror(errno));
			break;
		}
		readbuf_len += ret;
		if(ret < 512) {
			break;
		}
	}

	if(readbuf != NULL && readbuf_len > 0) {
		char *nextline = readbuf;
		while(nextline != NULL && nextline < readbuf + readbuf_len) {
			pid_t pid = -1;
			char *request = nextline;
			nextline = strchr(nextline, '\n');
			if(nextline != NULL) {
				*nextline++ = '\0';
			}
			if(strncmp(request, "enablenetwork", 13) == 0) {
				guest_enable_network();
				guest_reply(fd, request);
				continue;
			}
			if(strncmp(request, "disablenetwork", 14) == 0) {
				guest_disable_network();
				guest_reply(fd, request);
				continue;
			}

			sscanf(request, "%d", &pid);
			if(pid < 0) {
				dprintf(2, "failed to get pid\n");
				guest_reply(fd, request);
				continue;
			}

			if(guest_compare_namespace(pid) != 0) {
				dprintf(2, "pid %d not belongs to meetty environment\n", pid);
				guest_reply(fd, request);
				continue;
			}
			guest_network_add(pid);
			guest_cgroup_add(pid);
			guest_rlimit(pid);
			guest_reply(fd, request);
		}
	}
cleanup:
	if(readbuf != NULL) {
		free(readbuf);
		readbuf = NULL;
	}
	close(fd);
	return 0;
}

static void start() {
	struct sockaddr_un saddr = {};
	struct pollfd pfd = {};

	if((g.fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) {
		dprintf(2, "start: failed to create socket: %s\n", strerror(errno));
		exit(1);
	}

	saddr.sun_family = AF_UNIX;
	strncpy(saddr.sun_path, SOCKPATH, sizeof(saddr.sun_path) - 1);
	if(bind(g.fd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
		dprintf(2, "failed to bind: %s\n", strerror(errno));
		exit(1);
	}
	if(chmod(SOCKPATH, 0777) < 0) {
		dprintf(2, "failed to chmod: %s\n", strerror(errno));
		exit(1);
	}
	if(listen(g.fd, 128) < 0) {
		dprintf(2, "failed to chmod: %s\n", strerror(errno));
		exit(1);
	}

	pfd.fd = g.fd;
	pfd.events = POLLIN;
	while(!(poll(&pfd, 1, 1000) < 0)) {
		if(pfd.revents & POLLIN) {
			int fd = -1;
			pthread_t t;
			if((fd = accept(g.fd, NULL, NULL)) < 0) {
				dprintf(2, "accept failed: %s\n", strerror(errno));
				continue;
			}
			pthread_create(&t, NULL, (void*(*)(void*)) handle_request, (void *)(long) fd);
		}
	}

	if(g.fd > 0) {
		if(close(g.fd) < 0) {
			dprintf(2, "cleanup: failed to close sockfd properly: %s\n", strerror(errno));
		}
		g.fd = 0;
	}
}

static void guest_cgroup_mount(const char *dir, const char *controller) {
	if(access(dir, R_OK|W_OK|X_OK) != 0) {
		guest_cgroup_mkdir(dir);
		asprintf(&g.buf, "mount -t cgroup -o %s cgroup %s", controller, dir);
		if(system(g.buf) != 0) {
			dprintf(2, "guest_cgroup_mount: failed to mount %s: %s\n", controller, strerror(errno));
			exit(1);
		}
		free(g.buf);
		g.buf = NULL;
	}
}

static void guest_network_init(void) {
	asprintf(&g.buf, "grep %s /proc/self/net/dev >/dev/null 2>&1", BRIDGE);
	if(system(g.buf) != 0) {
		if(g.buf != NULL) {
			free(g.buf);
			g.buf = NULL;
		}

		asprintf(&g.buf, "ip link add %s type bridge", BRIDGE);
		system(g.buf);
		free(g.buf);
		g.buf = NULL;

		asprintf(&g.buf, "ip addr add %s dev %s", BRIDGE_ADDR, BRIDGE);
		system(g.buf);
		free(g.buf);
		g.buf = NULL;

		asprintf(&g.buf, "ip link set %s up", BRIDGE);
		system(g.buf);
		free(g.buf);
		g.buf = NULL;

		system("sysctl net.ipv4.ip_forward=1 >/dev/null");
	}

	if(g.buf != NULL) {
		free(g.buf);
		g.buf = NULL;
	}
}

static void guest_cgroup_init(void) {
	guest_cgroup_mount(GUEST_CGROUP_PIDS, "pids");
	guest_cgroup_mount(GUEST_CGROUP_MEMORY, "memory");
	guest_cgroup_mount(GUEST_CGROUP_CPU, "cpu,cpuacct");
}

static void sighandler(int sig) {
	guest_cleanup();
}

static void subscribe_to_signals() {
	signal(SIGTERM, sighandler);
	signal(SIGINT, sighandler);
}

int main(int argc, char *argv[]) {
	if(geteuid() != 0) {
		dprintf(2, "run this command as root\n");
		exit(1);
	}
	subscribe_to_signals();
	atexit(guest_cleanup);

	asprintf((char **)&g.usage, "%s [pids-max] [mem-max] [cpu-period-usec] [cpu-quota-usec] [net-bandwidth] [fd-max]", *argv++);
	if(*argv != NULL) {
		if(strncmp("-h", *argv, 2) == 0 || strncmp("--help", *argv, 6) == 0) {
			dprintf(2, "[usage] %s\n", g.usage);
			exit(0);
		}
		g.max_pids = *(argv++);
	}
	g.max_pids = (g.max_pids == NULL || g.max_pids[0] == '\0' ? DEFAULT_MAX_PIDS : g.max_pids);
	g.max_pids = strdup(g.max_pids);

	if(*argv != NULL) {
		g.max_mem = *(argv++);
	}
	g.max_mem = (g.max_mem == NULL || g.max_mem[0] == '\0' ? DEFAULT_MAX_MEM : g.max_mem);
	g.max_mem = strdup(g.max_mem);

	if(*argv != NULL) {
		g.cpu_period = *(argv++);
	}
	g.cpu_period = (g.cpu_period == NULL || g.cpu_period[0] == '\0' ? DEFAULT_CPU_PERIOD : g.cpu_period);
	g.cpu_period = strdup(g.cpu_period);

	if(*argv != NULL) {
		g.cpu_quota = *(argv++);
	}
	g.cpu_quota = (g.cpu_quota == NULL || g.cpu_quota[0] == '\0' ? DEFAULT_CPU_QUOTA : g.cpu_quota);
	g.cpu_quota = strdup(g.cpu_quota);

	if(*argv != NULL) {
		g.tc_bandwidth = *(argv++);
	}
	g.tc_bandwidth = (g.tc_bandwidth == NULL || g.tc_bandwidth[0] == '\0' ? DEFAULT_TC_BANDWIDTH : g.tc_bandwidth);
	g.tc_bandwidth = strdup(g.tc_bandwidth);

	if(*argv != NULL) {
		g.maxfd = *(argv++);
	}
	g.maxfd = (g.maxfd == NULL || g.maxfd[0] == '\0' ? DEFAULT_MAX_FD : g.maxfd);
	g.maxfd = strdup(g.maxfd);

	guest_network_init();
	guest_cgroup_init();
	start();
	guest_cleanup();
	return 0;
}
