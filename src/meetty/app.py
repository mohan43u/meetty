"""MeeTTY App.

Server app responsible for meetty website.
"""

import pty
import os
import sys
import signal
import termios
import struct
import fcntl
import time
import subprocess
from flask import Flask, render_template, request
from flask_socketio import SocketIO, emit
from gevent import monkey, select, Greenlet, signal_handler
from gevent.lock import BoundedSemaphore
monkey.patch_all()


app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins='*')
epoll = select.poll()
fds = {}
fds_lock = BoundedSemaphore()
sids = {}
default_trows = 24
default_tcols = 140
default_sleep = 5
default_wait = default_sleep


@app.route("/")
def index():
    """Index."""
    return render_template('index.html')


def resize(fd, rows, cols):
    """Resize the terminal based on provided parameters."""
    try:
        winsize = struct.pack("HHHH", rows, cols, 0, 0)
        fcntl.ioctl(fd, termios.TIOCSWINSZ, winsize)
    except Exception as error:
        print(f"resize: Exception: {error}")


def pty_size(fd):
    """Return the terminal size."""
    try:
        winsize_in = struct.pack("HHHH", 0, 0, 0, 0)
        winsize_out = fcntl.ioctl(fd, termios.TIOCGWINSZ, winsize_in)
        (rows, cols, xpixel, ypixel) = struct.unpack("HHHH", winsize_out)
        return (rows, cols)
    except Exception as error:
        print(f"pty_size: Exception: {error}")
        return (0, 0)


def close_ptys(master):
    """Close the terminals."""
    try:
        if (master not in fds) or (fds[master] is None):
            print(f"cleanup_ptys: no info for fd={master}")
        else:
            slavefd = fds[master]["slave"]
            if slavefd > 0:
                os.close(slavefd)
                fds[master]["slave"] = -1
            masterfd = fds[master]["master"]
            if masterfd > 0:
                os.close(masterfd)
                fds[master]["master"] = -1
    except Exception as error:
        print(f"close_ptys: Exception: {error}")


def connectthreadfunc(sid):
    """Handle new connection."""
    try:
        master = sids[sid]
        slave = fds[master]["slave"]
        namespace = fds[master]["namespace"]

        if namespace == '/nspawn':
            p = subprocess.Popen("unset TMUX; eval ${nspawncmdline}",
                                 shell=True,
                                 stdin=slave,
                                 stdout=slave,
                                 stderr=slave)
            fds[master]["p"] = p
            socketio.emit('pid', p.pid, namespace=namespace, room=sid)
            p.wait()
            print(f"nspawn: child {p.pid} terminated ")
            if (master in fds) and (fds[master] is not None):
                fds[master]["p"] = None
            close_ptys(master)

        elif namespace == '/tmux':
            (exitcode, output) = subprocess.getstatusoutput("eval ${tmuxtest}")
            if exitcode == 0:
                p = subprocess.Popen("unset TMUX; eval ${tmuxcmdline}",
                                     shell=True,
                                     stdin=slave,
                                     stdout=slave,
                                     stderr=slave)
                fds[master]["p"] = p
                p.wait()
                print(f"tmux: child {p.pid} terminated")
                if (master in fds) and (fds[master] is not None):
                    fds[master]["p"] = None
                close_ptys(master)
            else:
                p = subprocess.Popen("eval ${tmuxalt}",
                                     shell=True,
                                     stdin=subprocess.DEVNULL,
                                     stdout=slave,
                                     stderr=subprocess.DEVNULL)
                fds[master]["p"] = p
                while (exitcode != 0) and\
                      (master in fds) and\
                      (fds[master] is not None):
                    time.sleep(default_sleep)
                    (exitcode, output) = subprocess.getstatusoutput(
                        "eval ${tmuxtest}")

                p.terminate()
                p.wait(default_wait)
                p.kill()
                print(f"tmuxalt: child {p.pid} terminated")
                if (master in fds) and (fds[master] is not None):
                    fds[master]["p"] = None
                close_ptys(master)
    except Exception as error:
        print(f"connectthreadfunc: Exception: {error}")


@socketio.on('connect', namespace='/tmux')
@socketio.on('connect', namespace='/nspawn')
def connect():
    """Connect callback."""
    try:
        (master, slave) = pty.openpty()
        resize(master, default_trows, default_tcols)
        connectthread = Greenlet(connectthreadfunc, request.sid)
        info = {"sid": request.sid,
                "namespace": request.namespace,
                "connectthread": connectthread,
                "connectthread_started": False,
                "master": master,
                "slave": slave,
                "p": None}
        fds_lock.acquire()
        fds[master] = info
        fds_lock.release()
        sids[request.sid] = master
        epoll.register(master, select.POLLIN)
        print(f"connect: fd={master} sid={request.sid} " +
              f"namespace={request.namespace} connected")
    except Exception as error:
        print(f"connect: Exception: {error}")


def terminate(master):
    """Terminate the session."""
    try:
        if (master not in fds) or (fds[master] is None):
            print(f"terminate: no info for fd={master}")
        else:
            p = fds[master]["p"]
            if p is not None:
                p.terminate()
                p.wait(default_wait)
                p.kill()
                fds[master]["p"] = None
            else:
                print("terminate: not a valid p")
            close_ptys(master)
    except Exception as error:
        print(f"terminate: Exception: {error}")


def cleanup(sid, master):
    """Cleanup the file descriptors."""
    try:
        if master not in fds:
            print(f"cleanup: no info for fd={master}")
        else:
            terminate(master)
            unregister(master)
            del sids[sid]
    except Exception as error:
        print(f"cleanup: Exception: {error}")


@socketio.on('disconnect', namespace='/tmux')
@socketio.on('disconnect', namespace='/nspawn')
def disconnect():
    """Disconnect callback."""
    try:
        if (request.sid not in sids) or\
           (sids[request.sid] not in fds) or\
           (fds[sids[request.sid]] is None):
            print(f"disconnect: no details for {request.sid}")
        else:
            master = sids[request.sid]
            namespace = fds[master]["namespace"]
            sid = fds[master]["sid"]
            cleanup(sid, master)
            print(f"disconnect: fd={master} sid={sid} " +
                  f"namespace={namespace} disconnected")
    except Exception as error:
        print(f"disconnect: Exception: {error}")


@socketio.on('pty', namespace='/tmux')
@socketio.on('pty', namespace='/nspawn')
def handle_pty(string):
    """Pty request callback."""
    try:
        master = sids[request.sid]
        os.write(master, bytes(string, 'utf8'))
    except Exception as error:
        print(f"handle_pty: Exception: {error}")


@socketio.on('resize', namespace='/tmux')
@socketio.on('resize', namespace='/nspawn')
def handle_resize(rows, cols):
    """Resize callback."""
    try:
        master = sids[request.sid]
        info = fds[master]
        if info is not None:
            connectthread = info["connectthread"]
            connectthread_started = info["connectthread_started"]
            (trows, tcols) = pty_size(master)
            if request.namespace == "/nspawn":
                resize(master, rows, cols)
                trows = rows
                tcols = cols
            if request.namespace == "/tmux":
                if cols >= (tcols * 0.80):
                    resize(master, trows, cols)
                    tcols = cols
            if((connectthread is not None) and
               (connectthread_started is False)):
                connectthread.start()
                info["connectthread_started"] = True
            emit("resize", {"rows": trows, "cols": tcols})
        else:
            print("handle_resize: not a valid info")
    except Exception as error:
        print(f"handle_resize: Exception: {error}")


def unregister(fd):
    """Unregister the file descriptors from epoll."""
    try:
        if fd not in fds:
            print(f"unregister: no info for fd={fd}")
        else:
            epoll.unregister(fd)
            fds[fd] = None
    except Exception as error:
        print(f"unregister: Exception: {error}")


def iothreadfunc():
    """Epoll thread."""
    while True:
        results = epoll.poll(5)
        for fd, event in results:
            if (fd not in fds) or (fds[fd] is None):
                print(f"fd={fd} details not available")
                unregister(fd)
                continue

            namespace = fds[fd]["namespace"]
            sid = fds[fd]["sid"]
            if event & select.POLLIN:
                try:
                    data = os.read(fd, 1024 * 64)
                    socketio.emit('pty',
                                  data,
                                  namespace=namespace,
                                  room=sid)
                except Exception as error:
                    print(f"iothreadfunc: Exception while read: {error}")
                    unregister(fd)
                    socketio.emit('pty', (), namespace=namespace, room=sid)
                    print(f"iothreadfunc: fd={fd}: sid={sid} " +
                          f"namespace={namespace} event={event} sent EOF")
            else:
                unregister(fd)
                socketio.emit('pty', (), namespace=namespace, room=sid)
                print(f"iothreadfunc: fd={fd}: sid={sid} namespace={namespace}"
                      f" event={event} sent EOF")


def stop_socketio():
    """Stop socketio."""
    socketio.stop()


def shutdown():
    """Shutdown Meetty."""
    try:
        fds_lock.acquire()
        for master, info in fds.items():
            if info is not None:
                cleanup(info["sid"], master)
        fds_lock.release()
    except Exception as error:
        print(f"shutdown: error in shutdown {error}")


def run(*args, **kwargs):
    """Run the server."""
    signal_handler(signal.SIGTERM, stop_socketio)
    signal_handler(signal.SIGINT, stop_socketio)

    iothread = Greenlet(iothreadfunc)
    iothread.start()
    socketio.run(app, args[0], int(args[1]))
    iothread.kill()
    shutdown()
    print("finished")


if __name__ == '__main__':
    run(*sys.argv[1:])
